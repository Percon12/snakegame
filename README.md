# SnakeGame

Esse é o classico jogo snake com algumas modificações, todo feito em java utilizando os kits de ferramentas do AWT e SWING.

## Informações

Temos aqui uma nova peculiaridade, tipos de maçã diferentes!

-Maçã Big(Laranja). Dobro de pontos.

-Maçã Decrease (Rosa). Reinicia o tamanho da cobra.

-Maçã Bomb (Cinza Escuro). Mata a cobra.

### Pré requisitos

Para poder jogar esse fabuloso e classico jogo você terá que baixa-lo e ter uma alguma versão do java instalada no computador (recomenda-se as versões 8 ou 11).
segue abaixo o passo a passo para jogar!

### Instalando

primeiro copie o link do repositorio (na parte superior direita da tela) para clone no seu computador a partir do comando

```
git clone https://gitlab.com/FelipeCorreiaAndrade/snakegame
```

## Rodando o programa

Pronto, agora acesse a pasta onde foi clonado o aquivo e execute o jogo a partir do arquivo chamado

```
SnakeGame.exe
```

OBS: Você tambem pode executar o programa pela própria IDE onde você abre o projeto e clica em run, até mesmo pelo terminal.

## Autor

* **Felipe Correia** - (https://github.com/FelipeCorreiaAndrade)

OBS: Acabei tendo vários problemas com o meu computador e com a IDE e não consegui mexer no programa mas..

ja tinha implementado os pontos na tela (tanto que está até na programação do jogo) e algumas coisas a mais.. porêm deu um erro que não encontrei solução com ninguem.

desculpe.
