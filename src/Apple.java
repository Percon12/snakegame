import java.awt.Color;
import java.awt.Graphics;

public class Apple {

	private int xcoord, ycoord, largura, altura, tipoApple;
	
	public Apple(int xcoord, int ycoord, int tileSize, int tipoApple){
 		this.xcoord = xcoord;
		this.ycoord = ycoord;
		this.tipoApple = tipoApple;
		largura = tileSize;
		altura = tileSize;
	}
	
		
	public int getTipoApple() {
		return tipoApple;
	}


	public void setTipoApple(int tipoApple) {
		this.tipoApple = tipoApple;
	}


	public void tick() {
		
	}
	

	public int getXcoord() {
		return xcoord;
	}

	public void setXcoord(int xcoord) { 
		this.xcoord = xcoord;
	}

	public int getYcoord() {
		return ycoord;
	}

	public void setYcoord(int ycoord) {
		this.ycoord = ycoord;
	}
	
	public void draw(Graphics g) {
		//NORMAL
		if(tipoApple == 1) {
			g.setColor(Color.RED);
			g.fillRect(xcoord * largura, ycoord * altura, largura, altura);
		}
		//DOBRO DE PONTOS
		else if(tipoApple == 2) {
			g.setColor(Color.ORANGE);
			g.fillRect(xcoord * largura, ycoord * altura, largura, altura);
		}
		//RESETA O TAMANHO E OS PONTOS DA COBRA
		else if(tipoApple == 3) {
			g.setColor(Color.PINK);
			g.fillRect(xcoord * largura, ycoord * altura, largura, altura);
		}
		//MATA A COBRA
		else if(tipoApple == 4) {
			g.setColor(Color.DARK_GRAY);
			g.fillRect(xcoord * largura, ycoord * altura, largura, altura);
		}
		
	}
}

	
	


