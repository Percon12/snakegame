import java.awt.Color;
import java.awt.Dimension;

import java.awt.Graphics;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JPanel;
import javax.swing.JOptionPane;
 
public class Painel extends JPanel implements Runnable, KeyListener {

	private static final long serialVersionUID = 1L;
	
	public static final int LARGURA = 500, ALTURA = 500;
	
	private Thread thread;
	
	private boolean running;
	
	private boolean right = true, left = false, up = false, down = false;
	
	private ParteDoCorpo b;
	private ArrayList<ParteDoCorpo> snake;
	
	private Apple apple;
	private ArrayList<Apple> apples;
	private Apple especial;
	private ArrayList<Apple> powerup;
	
	private Random r;
	private Random tipoPower;
	private Random p;


	private int pts = 0;
	
	private int tipoApple;
	
	private int xcoord = 10, ycoord = 10, size = 5;
	private int ticks = 0;
	
	public Painel() {
		
		setFocusable(true);
		
		setPreferredSize(new Dimension(LARGURA, ALTURA));
		addKeyListener(this);
		
		snake = new ArrayList<ParteDoCorpo>();
		apples = new ArrayList<Apple>();
		powerup = new ArrayList<Apple>();
						
		r = new Random();
		p = new Random();	
		tipoPower = new Random();

		start();
		
	}
	
	public void start(){
		running = true;
		thread = new Thread(this);
		thread.start();
		
	}
	public void stop() {
		running = false;
		try {
			thread.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void restart() {
        xcoord = 10;
        ycoord = 10;
        right = true;
        left = false;
        down = false;
        up = false;
        size = 5;
        pts = 0;
        snake.clear();
        apples = new ArrayList<Apple>();
        powerup = new ArrayList<Apple>();
        b = new ParteDoCorpo(xcoord, ycoord, 10);
        snake.add(b);
        apple = new Apple(xcoord + 1, ycoord, 10, 1);
        especial = new Apple(xcoord + 2, ycoord, 10, 3);
        apples.add(apple);
        powerup.add(especial);
    }
	public void tick() {
		if (snake.size() == 0) {
			b = new ParteDoCorpo(xcoord , ycoord , 10);
			snake.add(b);
		}
		ticks++;
		if(ticks > 700000) {
			if(right) xcoord++;
			if(left) xcoord--;
			if(up) ycoord--;
			if(down) ycoord++;
			
			ticks = 0;
			
			b = new ParteDoCorpo(xcoord , ycoord , 10);
			snake.add(b);
			
			if(snake.size() > size) {
				snake.remove(0);
			}
			if(apples.size() == 0) {
				int xcoord = r.nextInt(49);
				int ycoord = r.nextInt(49);
				
				apple = new Apple(xcoord, ycoord, 10, 1);
				apples.add(apple);
			}
			
			if(powerup.size() == 0) {
				int tipo_Power = tipoPower.nextInt(4 - 2 + 1) + 2;
				
				int xcoord2 = p.nextInt(49);
				int ycoord2 = p.nextInt(49);
				
				especial = new Apple(xcoord2, ycoord2, 10, tipo_Power);
				powerup.add(especial);
				}
			
			for(int i = 0 ; i < apples.size() ; i++) {
				if(xcoord == apples.get(i).getXcoord() && ycoord == apples.get(i).getYcoord()) {
					size++;
					pts++;
					apples.remove(i);
					i++;
				}
			}
				for (int i1 = 0; i1 < powerup.size(); i1++) {
					if(xcoord == powerup.get(i1).getXcoord() && ycoord == powerup.get(i1).getYcoord()) {
						if(powerup.get(i1).getTipoApple() == 2) {
							size++;
							pts += 2;
							powerup.remove(i1);
							i1++; 	
						}
						else if(powerup.get(i1).getTipoApple() == 3) {
							snake.clear();
							size = 5;
							pts = 0;
							b = new ParteDoCorpo(xcoord, ycoord, 10);
							snake.add(b);
							powerup.remove(i1);
							i1++;	
						}
						else if(powerup.get(i1).getTipoApple() == 4) {
							System.out.printf("Sua pontua��o foi: ");
		                	System.out.print(pts);
		                	System.out.print("\n");
							int answer2 = JOptionPane.showConfirmDialog(null , "Retry?");
		                	powerup.remove(i1);
							i1++;
		                    if (answer2 == 0) {
		                        restart();
		                    } else if(answer2 == 1) {
		                        System.exit(0);
		                    } 
		                    else {
		                        stop();	
						}
				}
			}
			//Colis�o com o corpo	
			for(int i2 = 0; i2 < snake.size(); i2++) {
	            if(xcoord == snake.get(i2).getXcoord() && ycoord == snake.get(i2).getYcoord()) {
	                if(i2 != snake.size() - 1) {
	                	System.out.printf("Sua pontua��o foi: ");
	                	System.out.print(pts);
	                	System.out.print("\n");
	                	
	                    int answer = JOptionPane.showConfirmDialog(null , "Retry?");
	                    if (answer == 0) {
	                        restart();
	                    } else if(answer == 1) {
	                        System.exit(0);
	                    } 
	                    else {
	                        stop();
	                    }
	                }
	            }
	        }

	        if (xcoord < 0 || xcoord > 49 || ycoord < 0 || ycoord > 49) {
	        	System.out.printf("Sua pontua��o foi: ");
	        	System.out.println(pts);
	        	System.out.print("\n");
	            int answer = JOptionPane.showConfirmDialog(null, "Retry?");	            
	            if (answer == 0) {
	                restart();
	            } else if(answer == 1) {
	                    System.exit(0);
	            } else {
	                stop();
	            }
	        }
			//Colis�o com a borda
			if(xcoord < 0 || xcoord > 49 || ycoord < 0 || ycoord >49) {
				stop();
			}
		}
	}
}
	public void paint(Graphics g) {
		
		g.fillRect(0, 0, LARGURA, ALTURA);
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, LARGURA, ALTURA);
		
		for(int i = 0 ; i < LARGURA/10 ; i++) {
			g.drawLine(i * 10, 0, i * 10, ALTURA);
		}	
		for(int i = 0 ; i < ALTURA/10 ; i++) {
			g.drawLine(0, i * 10, ALTURA, i * 10);
		}	
		for(int i = 0 ; i < snake.size() ; i++) {
			snake.get(i).draw(g);
		}
		for(int i = 0 ; i < apples.size() ; i++) {
			apples.get(i).draw(g);
		}
		for (int i = 0; i < powerup.size(); i++) {
			powerup.get(i).draw(g);
		}

	}
	
	@Override
	public void run() {
		while(running) {
			tick();
			repaint();
		}
		
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	@Override
	public void keyPressed(KeyEvent e) {
		int key = e.getKeyCode();
		if (key == KeyEvent.VK_RIGHT && !left) {
			right = true;
			up = false;
			down = false;
		}
		if (key == KeyEvent.VK_LEFT && !right) {
			left = true;
			up = false;
			down = false;
		}
		if (key == KeyEvent.VK_UP && !down) {
			up = true;
			left = false;
			right = false;
		}
		if (key == KeyEvent.VK_DOWN && !up) {
			down = true;
			left = false;
			right = false;
		}
	}
	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	public int getTipoApple() {
		return tipoApple;
	}

	public void setTipoApple(int tipoApple) {
		this.tipoApple = tipoApple;
	}
	
}
